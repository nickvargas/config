:colorscheme darkblue

"tab characters are for the weak minded
set autoindent
set smartindent
set sw=3
set tabstop=3
set expandtab
set number

"make it pretty(er)
set ruler
set rulerformat=%55(%{strftime('%a\ %b\ %e\ %I:%M\ %p')}\ %5l,%-6(%c%V%)\ %P%)

"tab config
nnoremap <c-t> :tabe<cr>
nnoremap qq :q<cr>
nnoremap <c-k> gt
nnoremap <c-j> gT

"make <CR> insert newlines
nmap <CR> o<Esc>

"swap j and k (#sorryboutit)
nnoremap j k
nnoremap k j
vnoremap j k
vnoremap k j

"make shift repeat navigation 10 times
nnoremap <S-J> 10k
nnoremap <S-K> 10j
nnoremap <S-H> 10h
nnoremap <S-L> 10l

"the tab key implies insertion. HEEYOOO
nnoremap <tab> i<tab>

"ignore case with q
command Q q

"auto-match curly brace
inoremap {<cr> {<cr>}<c-o>O
