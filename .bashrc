export PS1="\[\033[32m\][\w]\[\033[0m\]\n\[\033[1;36m\]\u\[\033[m\]@\[\033[32m\]\h\[\033[1;33m\]-> \[\033[0m\]" #use custom prompt
set horizontal-scroll-mode on
export cmps="/afs/cats.ucsc.edu/courses/cmps012b-wm" #make shortcut
export cmpsa="$cmps/Assignments"
export cmpsl="$cmps/Labs-cmps012m"
source ~/.bash_aliases
export PATH=$PATH:/afs/cats.ucsc.edu/courses/cmps012b-wm/bin

