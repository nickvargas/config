
alias ls='ls -h --color=auto'
alias ll='ls -l'
alias lla='ls -la'

alias grep='grep --color'

alias cls='clear'

alias ..='cd ..'
alias ...='cd ../../'

alias :q='exit'

alias pp='python'

# Git:
alias gs='git status'
alias gcom='git commit -a'

# CMPS012b-wm.f13:
alias subcmps='submit cmps012b-wm.f13'
alias subtest='~/scripts/testsub'

# Fun:
alias study='python ~/scripts/asciiart/koolprint.py'
alias .,.,='~/.snow/snow.sh'
alias please='sudo'

#ssh
alias ssh_rasp='ssh -p 5005 pi@ssh.nickvargas.net'
alias ssh_cent='ssh -p 5050 nick@ssh.nickvargas.net'
alias ssh_ucsc='ssh niavarga@unix.ucsc.edu'
